package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.dtos.AccountDTO;
import com.EnergyUtilityApp.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountControllerFail.class);


    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


    @GetMapping()
    public ResponseEntity<List<AccountDTO>> getAccounts() {
        System.out.println("\n\n Intrat in get");
        List<AccountDTO> dtos = accountService.findAccounts();
        for (AccountDTO dto : dtos) {
            Link accountLink = linkTo(methodOn(AccountController.class)
                    .getAccount((dto.getId()))).withRel("accountDetaild");
            dto.add(accountLink);
        }

        LOGGER.info("This was called with dto {}", dtos);
        LOGGER.error("This was called with ");

        System.out.println("\n\n\n\n\nTestam old school \n\n\n\n\n");

        System.out.println(dtos);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AccountDTO> getAccount(@PathVariable("id") UUID accountId) {
        AccountDTO dto = accountService.findAccountById(accountId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertAccount(@Valid @RequestBody AccountDTO accountDTO) {
        UUID accountID = accountService.insert(accountDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<AccountDTO> deleteAccount(@PathVariable("id") UUID accountId) {
        AccountDTO dto = accountService.deleteById(accountId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<AccountDTO> updateAccount(@PathVariable("id") UUID accountId, @Valid @RequestBody AccountDTO accountDTO) {
        LOGGER.error("We updated the account ");
        AccountDTO updatedAccount = accountService.update(accountId, accountDTO);
        return new ResponseEntity<>(updatedAccount, HttpStatus.OK);
    }
}
