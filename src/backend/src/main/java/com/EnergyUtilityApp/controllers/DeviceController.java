package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.dtos.DeviceDTO;
import com.EnergyUtilityApp.services.DeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//todo try to have device/{idClient} in order to remove the idClient from dto
//todo ask how to do the security ( from react or from springSecurity)

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DeviceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceController.class);

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getDevice() {
        List<DeviceDTO> dtos = deviceService.findDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice((dto.getId()))).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping("/empty")
    public ResponseEntity<List<DeviceDTO>> getEmptyDevice() {
        List<DeviceDTO> dtos = deviceService.findEmptyDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice((dto.getId()))).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable("id") UUID deviceID) {
        DeviceDTO dto = deviceService.findDeviceById(deviceID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "account/{id}")
    public ResponseEntity<List<DeviceDTO>> getDeviceByAccount(@PathVariable("id") UUID accountId) {
        List<DeviceDTO> dtos = deviceService.findDeviceByAccountId(accountId);
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice((dto.getId()))).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "account/name/{name}")
    public ResponseEntity<List<DeviceDTO>> getDeviceByAccountName(@PathVariable("name") String accountName) {
        List<DeviceDTO> dtos = deviceService.findDeviceByAccountName(accountName);
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(methodOn(DeviceController.class)
                    .getDevice((dto.getId()))).withRel("deviceDetails");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertDevice(@Valid @RequestBody DeviceDTO deviceDTO) {
        UUID deviceId = deviceService.insert(deviceDTO);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);
    }

//todo folosit asta pentru insert , de forma device/{userId}
    @PostMapping(value = "/{id}")
    public ResponseEntity<UUID> insertDeviceWithUserId(@Valid @RequestBody DeviceDTO deviceDTO , @PathVariable("id") UUID accountId) {
        UUID accountID = deviceService.insertWithUserId(deviceDTO,accountId);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> deleteDevice(@PathVariable("id") UUID deviceId)  {
        DeviceDTO dto = deviceService.deleteById(deviceId);
        return new ResponseEntity<DeviceDTO>(dto, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> updateDTO(@PathVariable("id") UUID deviceId, @Valid @RequestBody DeviceDTO deviceDTO) {
        DeviceDTO updateDevice = deviceService.update(deviceId, deviceDTO);
        return new ResponseEntity<>(updateDevice, HttpStatus.OK);
    }
}
