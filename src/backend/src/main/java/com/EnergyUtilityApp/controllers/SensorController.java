package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.dtos.SensorDTO;
import com.EnergyUtilityApp.services.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * The class  SensorController
 *
 * @author Cofaru Vlad
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/sensor")
public class SensorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorController.class);

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorDTO>> getSensort() {
        List<SensorDTO> dtos = sensorService.findSensors();
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorController.class)
                    .getSensor((dto.getId()))).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> getSensor(@PathVariable("id") UUID deviceID) {
        SensorDTO dto = sensorService.findSensorById(deviceID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "account/{id}")
    public ResponseEntity<List<SensorDTO>> getSensorByAccountId(@PathVariable("id") UUID accountId) {
        List<SensorDTO> dtos = sensorService.findSensorByAccountIdId(accountId);
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorController.class)
                    .getSensor((dto.getId()))).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertSensor(@Valid @RequestBody SensorDTO sensorDTO) {
        UUID sensorID = sensorService.insert(sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<UUID> insertSensor(@Valid @RequestBody SensorDTO sensorDTO,@PathVariable("id") UUID deviceId) {
        UUID sensorID = sensorService.insertWithDevice(deviceId,sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> deleteSensor(@PathVariable("id") UUID sensorId) {
        SensorDTO dto = sensorService.deleteById(sensorId);
        return new ResponseEntity<SensorDTO>(dto, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> updateDTO(@PathVariable("id") UUID sensorId, @Valid @RequestBody SensorDTO sensorDTO) {
        SensorDTO updateSensor = sensorService.update(sensorId, sensorDTO);
        return new ResponseEntity<>(updateSensor, HttpStatus.OK);
    }
}
