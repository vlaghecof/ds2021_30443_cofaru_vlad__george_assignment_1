package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountControllerFail {
    private final AccountService accountService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountControllerFail.class);


    @Autowired
    public AccountControllerFail(AccountService accountService) {
        this.accountService = accountService;
    }


/*    @GetMapping()
    public ResponseEntity<List<AccountDTO>> getAccounts() {
        System.out.println("\n\n Intrat in get");
        List<AccountDTO> dtos = accountService.findAccounts();
        for (AccountDTO dto : dtos) {
            Link accountLink = linkTo(methodOn(AccountController.class)
                    .getAccount(UUID.fromString(dto.getId()))).withRel("accountDetaild");
            dto.add(accountLink);
        }

        LOGGER.info("This was called with dto {}", dtos);
        LOGGER.error("This was called with ");

        System.out.println("\n\n\n\n\nTestam old school \n\n\n\n\n");

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

//    @GetMapping(value = "/{id}")
//    public ResponseEntity<AccountDTO> getAccount(@PathVariable("id") UUID accountId) {
//        AccountDTO dto = accountService.findAccountById(accountId);
//
//        LOGGER.info("This get by Id was called with with {}", accountId);
//        LOGGER.error("This was called from get by id  ");
//
//        System.out.println("\n\n\n\n\nTestam old school \n\n\n\n\n");
//
//        return new ResponseEntity<>(dto, HttpStatus.OK);
//    }
//
//    @PostMapping()
//    public ResponseEntity<UUID> insertAccount(@Valid @RequestBody AccountDTO accountDTO) {
//        UUID accountID = accountService.insert(accountDTO);
//        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity<AccountDTO> deleteAccount(@PathVariable("id") UUID accountId) {
//        AccountDTO dto = accountService.deleteById(accountId);
//        return new ResponseEntity<>(dto, HttpStatus.OK);
//    }
//
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<AccountDTO> updateAccount(@PathVariable("id") UUID accountId, @Valid @RequestBody AccountDTO accountDTO) {
//        AccountDTO updatedAccount = accountService.update(accountId, accountDTO);
//        return new ResponseEntity<>(updatedAccount, HttpStatus.OK);
//    }

}

