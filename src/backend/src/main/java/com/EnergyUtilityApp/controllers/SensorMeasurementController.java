package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.dtos.SensorDTO;
import com.EnergyUtilityApp.dtos.SensorMeasurementDTO;
import com.EnergyUtilityApp.services.SensorMeasurementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * The class  SensorMeasurementController
 *
 * @author Cofaru Vlad
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/measurement")
public class SensorMeasurementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorMeasurementController.class);

    private final SensorMeasurementService sensorMeasurementService;

    @Autowired
    public SensorMeasurementController(SensorMeasurementService sensorMeasurementService) {
        this.sensorMeasurementService = sensorMeasurementService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorMeasurementDTO>> getSensorMeasurements() {

        LocalDateTime lt
                = LocalDateTime.now();
        LOGGER.info("Current time " , lt);
        List<SensorMeasurementDTO> dtos = sensorMeasurementService.findSensorMeasurement();
        for (SensorMeasurementDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorMeasurementController.class)
                    .getSensorMeasurement((dto.getId()))).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/generate")
    public ResponseEntity<List<SensorMeasurementDTO>> generateSensorMeasurements() {

        LocalDateTime lt
                = LocalDateTime.now();
        LOGGER.info("Current time " , lt);

        List<SensorMeasurementDTO> dtos = sensorMeasurementService.findSensorMeasurement();
        for (SensorMeasurementDTO dto : dtos) {
            Link sensorLink = linkTo(methodOn(SensorMeasurementController.class)
                    .getSensorMeasurement((dto.getId()))).withRel("sensorDetails");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorMeasurementDTO> getSensorMeasurement(@PathVariable("id") UUID deviceID) {
        SensorMeasurementDTO dto = sensorMeasurementService.findSensorMeasurementByID(deviceID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "device/{id}")
    public ResponseEntity<  List<SensorMeasurementDTO>> getSensorMeasurementByDevice(@PathVariable("id") UUID deviceID) {
        List<SensorMeasurementDTO> dto = sensorMeasurementService.findSensorMeasurementByDeviceId(deviceID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    //todo fa sa fie mereu current timestamp
    @PostMapping()
    public ResponseEntity<UUID> insertSensorMeasurement(@Valid @RequestBody SensorMeasurementDTO sensorDTO) {
        UUID sensorID = sensorMeasurementService.insert(sensorDTO);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }


    //todo use this for the update of the form measurement/{sensorId}

    /**
     * Method used to insert a measurement to the sensor with id sensorId
     * @param sensorDTO
     * @param sensorId
     * @return
     */
    @PostMapping(value = "/{id}")
    public ResponseEntity<UUID> insertSensorMeasurementWithDeviceId(@Valid @RequestBody SensorMeasurementDTO sensorDTO,@PathVariable("id") UUID sensorId) {
        LocalDateTime lt
                = LocalDateTime.now();
        sensorDTO.setTimestamp(lt);
        UUID sensorID = sensorMeasurementService.insertWithSensorId(sensorDTO,sensorId);
        return new ResponseEntity<>(sensorID, HttpStatus.CREATED);
    }

    /**
    //     * Method used to insert a generate a set of 10 measurements for the sensor with sensorId
    //     * @param sensorDTO
    //     * @param sensorId
    //     * @return
    //     */
    @GetMapping(value = "generate/{id}")
    public ResponseEntity<UUID> generateSensorMeasurementWithDeviceId(@PathVariable("id") UUID sensorId) {
        LocalDateTime lt
                = LocalDateTime.now();

        Random rand = new Random();


        for(int i=-5;i<5;i++)
        {   System.out.println(lt.minusHours(i));
            int consumption = rand.nextInt(100);
            System.out.println(consumption);
            SensorMeasurementDTO sensorMeasurementDTO = SensorMeasurementDTO.builder().timestamp(lt.minusHours(i)).energyConsumption(consumption).build();
            System.out.println(sensorMeasurementDTO);
            sensorMeasurementService.insertWithSensorId(sensorMeasurementDTO,sensorId);
        }

//        sensorDTO.setTimestamp(lt);
//        SensorMeasurementDTO sensorMeasurementDTO = SensorMeasurementDTO.builder().timestamp(lt).energyConsumption(44).build();
//        UUID sensorID = sensorMeasurementService.insertWithSensorId(sensorDTO,sensorId);
          return new ResponseEntity<>(sensorId, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<SensorMeasurementDTO> deleteSensorMeasurement(@PathVariable("id") UUID sensorId) {
        SensorMeasurementDTO dto = sensorMeasurementService.deleteById(sensorId);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<SensorMeasurementDTO> updateDTO(@PathVariable("id") UUID sensorId, @Valid @RequestBody SensorMeasurementDTO sensorDTO) {
        SensorMeasurementDTO updateSensor = sensorMeasurementService.update(sensorId, sensorDTO);
        return new ResponseEntity<>(updateSensor, HttpStatus.OK);
    }

}
