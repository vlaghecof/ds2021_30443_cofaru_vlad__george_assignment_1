package com.EnergyUtilityApp.controllers;

import com.EnergyUtilityApp.configure.JwtTokenUtil;
import com.EnergyUtilityApp.dtos.AccountDTO;
import com.EnergyUtilityApp.entities.JWTResponse;
import com.EnergyUtilityApp.services.AccountDetailService;
import com.EnergyUtilityApp.services.AccountDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * The class  JwtAuthenticationController
 *
 * @author Cofaru Vlad
 */
@RestController
@CrossOrigin
public class JwtAuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final AccountDetailService jwtUserDetailsService;

    @Autowired
    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, AccountDetailService jwtUserDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.jwtUserDetailsService = jwtUserDetailsService;
    }


    @PostMapping(value = "/authenticate")
    public ResponseEntity<JWTResponse> createAuthenticationToken(@RequestBody AccountDTO authenticationRequest)
            throws Exception {

        this.authenticate(authenticationRequest.getName(), authenticationRequest.getPassword());

        final AccountDetails userDetails = jwtUserDetailsService
                .loadUserByUsername(authenticationRequest.getName());

        final String token = jwtTokenUtil.generateToken(userDetails);

        JWTResponse test = new JWTResponse(token,userDetails.getUsername());
        return new ResponseEntity<>(new JWTResponse(token,userDetails.getUsername(),userDetails.getUserId(),userDetails.isAdmin()), HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
