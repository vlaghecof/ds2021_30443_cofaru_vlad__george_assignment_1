package com.EnergyUtilityApp.repositories;

import com.EnergyUtilityApp.entities.SensorMeasurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;
import java.util.List;
public interface SensorMeasurementRepository extends JpaRepository<SensorMeasurement, UUID> {

    List<SensorMeasurement> findBySensor_Id(UUID sensorID);


    List<SensorMeasurement> findBySensor_SensorDevice_Id(UUID deviceId);

//    select * from measurements where sensor-id = (select id from sensor where device-id=:id)

//    @Query(value = "SELECT p " +
//            "FROM SensorMeasurement p " +
//            "WHERE p.sensor.id = " +
//            "( SELECT s.id from Sensor s where s.sensorDevice.id=:id )")
//    List<SensorMeasurement>findBySensor_SensorDevice_Id(@Param("id") UUID deviceId);
}
