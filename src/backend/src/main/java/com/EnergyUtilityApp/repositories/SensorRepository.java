package com.EnergyUtilityApp.repositories;

import com.EnergyUtilityApp.entities.Person;
import com.EnergyUtilityApp.entities.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;
import java.util.List;
public interface SensorRepository extends JpaRepository<Sensor, UUID> {

    List<Sensor> findBySensorDevice_Account_Id(UUID accountId);
}
