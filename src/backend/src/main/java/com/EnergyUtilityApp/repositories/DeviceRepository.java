package com.EnergyUtilityApp.repositories;

import com.EnergyUtilityApp.entities.Device;
import com.EnergyUtilityApp.entities.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;
import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, UUID> {

    List<Device> findByAccount_Id(UUID accountId);


    /**
     * Example: Write Custom Query
     */
    @Query(value = "From Device d  where\n" +
            "    d.id not in( Select s.sensorDevice from Sensor s)")
    List<Device> getEmptyDevices();

    List<Device> getDeviceByAccount_Id(UUID accountId);
//    List<Device> get
    List<Device> getDeviceByAccount_Name(String name);
}
