package com.EnergyUtilityApp.repositories;

import com.EnergyUtilityApp.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountRepository  extends JpaRepository<Account, UUID> {

    Account findByName(String name);

}
