package com.EnergyUtilityApp.entities;

import java.io.Serializable;
import java.util.UUID;

/**
 * The class  JWTResponse
 *
 * @author Cofaru Vlad
 */
public class JWTResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private String name;
    private UUID accoutId;
    private boolean isAdmin;

    public JWTResponse(String jwttoken, String name, UUID accoutId, boolean isAdmin) {
        this.jwttoken = jwttoken;
        this.name = name;
        this.accoutId = accoutId;
        this.isAdmin = isAdmin;
    }

    public JWTResponse(String jwttoken, String name, UUID accoutId) {
        this.jwttoken = jwttoken;
        this.name = name;
        this.accoutId = accoutId;
    }

    public JWTResponse(String jwttoken, String name) {
        this.jwttoken = jwttoken;
        this.name = name;
    }

    public JWTResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return this.jwttoken;
    }

    public String getName() {
        return this.name;
    }

    public UUID getId() {
        return this.accoutId;
    }

    public boolean isAdmin() {
        return isAdmin;
    }
}
