package com.EnergyUtilityApp.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SensorMeasurement implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "time_stamp", nullable = false)
    private LocalDateTime timestamp;

    @Column(name = "energyConsumption", nullable = false)
    private Integer energyConsumption;

    @ManyToOne
    @JoinColumn(name="sensor_id", nullable=false)
    private Sensor sensor;

    @Override
    public String toString() {
        return "SensorMeasurement{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", energyConsumption=" + energyConsumption +
                '}';
    }
}
