package com.EnergyUtilityApp.dtos.builders;

import com.EnergyUtilityApp.dtos.AccountDTO;
import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.entities.Device;
import lombok.NoArgsConstructor;

import java.util.UUID;
import java.util.List;

@NoArgsConstructor
public class AccountBuilder {

    public static AccountDTO toAccountDto (Account account)
    {
        return  AccountDTO.builder()
                .id(account.getId())
                .name(account.getName())
                .birthDate(account.getBirthDate())
                .password(account.getPassword())
                .isAdmin(account.getIsAdmin())
                .address(account.getAddress())
                .deviceList(account.getDeviceList().toString())
                .build();
    }

    public static Account toEntity(AccountDTO accountDTO , List<Device> devices)
    {
        return Account.builder()
                .id((accountDTO.getId()))
                .name(accountDTO.getName())
                .birthDate(accountDTO.getBirthDate())
                .password(accountDTO.getPassword())
                .isAdmin(accountDTO.getIsAdmin())
                .address(accountDTO.getAddress())
                .deviceList(devices)
                .build();
    }
}
