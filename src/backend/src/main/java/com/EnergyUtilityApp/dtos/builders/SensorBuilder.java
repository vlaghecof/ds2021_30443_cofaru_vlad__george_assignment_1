package com.EnergyUtilityApp.dtos.builders;

import com.EnergyUtilityApp.dtos.DeviceDTO;
import com.EnergyUtilityApp.dtos.SensorDTO;
import com.EnergyUtilityApp.entities.Device;
import com.EnergyUtilityApp.entities.Sensor;
import com.EnergyUtilityApp.entities.SensorMeasurement;
import lombok.NoArgsConstructor;
import java.util.List;
/**
 * The class  SensorBuilder
 *
 * @author Cofaru Vlad
 */
@NoArgsConstructor

public class SensorBuilder {

    public static SensorDTO toSensorDTO(Sensor sensor) {
        return SensorDTO.builder()
                .id(sensor.getId())
                .maximumValue(sensor.getMaximumValue())
                .sensorDescription(sensor.getSensorDescription())
                .deviceId(sensor.getSensorDevice().getId())
                .measurements(sensor.getMeasurements().toString())
                .build();
    }

    public static Sensor toEntity(SensorDTO sensorDTO, Device sensorDevice, List<SensorMeasurement> measurementList)
    {
        return Sensor.builder()
                .id(sensorDTO.getId())
                .maximumValue(sensorDTO.getMaximumValue())
                .sensorDescription(sensorDTO.getSensorDescription())
                .sensorDevice(sensorDevice)
                .Measurements(measurementList)
                .build();
    }

}
