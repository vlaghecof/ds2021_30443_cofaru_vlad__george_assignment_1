package com.EnergyUtilityApp.dtos;

import com.EnergyUtilityApp.entities.Device;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

/**
 * The class  SensorDTO
 *
 * @author Cofaru Vlad
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class SensorDTO extends RepresentationModel<SensorDTO> {

    private UUID id;

    private String sensorDescription;

    private Integer maximumValue;

    private UUID deviceId;

    private String measurements ;
}
