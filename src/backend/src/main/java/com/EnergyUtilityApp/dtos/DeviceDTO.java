package com.EnergyUtilityApp.dtos;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class DeviceDTO extends RepresentationModel<DeviceDTO> {

    private UUID id;

    private Integer maximumConsumption;

    private String description;

    private String location;

    private Integer averageConsumption;

//    private String monitoringSensor;

    private UUID accountId;

}
