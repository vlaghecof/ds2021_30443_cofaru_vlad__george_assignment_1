package com.EnergyUtilityApp.dtos;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class AccountDTO extends RepresentationModel<AccountDTO> {

    private UUID id;

    private String name;

    private String password;

    private LocalDate birthDate;

    private String address;

    private Boolean isAdmin;

    //    private List<Device> deviceList;
    private String deviceList;


}
