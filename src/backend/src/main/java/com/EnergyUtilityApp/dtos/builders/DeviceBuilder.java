package com.EnergyUtilityApp.dtos.builders;

import com.EnergyUtilityApp.dtos.DeviceDTO;
import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.entities.Device;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DeviceBuilder {

    public static DeviceDTO toDeviceDto(Device device) {
        return DeviceDTO.builder()
                .id(device.getId())
                .location(device.getLocation())
                .description(device.getDescription())
                .averageConsumption(device.getAverageConsumption())
                .maximumConsumption(device.getMaximumConsumption())
                .accountId(device.getAccount().getId())
//                .accountName(device.getAccount().getName())
                .build();
    }


    public static Device toEntity(DeviceDTO deviceDTO, Account account) {
        return Device.builder()
                .id(deviceDTO.getId())
                .location(deviceDTO.getLocation())
                .description(deviceDTO.getDescription())
                .averageConsumption(deviceDTO.getAverageConsumption())
                .maximumConsumption(deviceDTO.getMaximumConsumption())
                .account(account)
                .build();
    }

}
