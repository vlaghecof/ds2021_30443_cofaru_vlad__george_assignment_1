package com.EnergyUtilityApp.services;

import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * The class  AccountDetailService
 *
 * @author Cofaru Vlad
 */
@Service
public class  AccountDetailService implements UserDetailsService {

    private final AccountRepository accountRepository;


    @Autowired
    public AccountDetailService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Account account = accountRepository.findByName(userName);
        AccountDetails accountDetails = new AccountDetails(account);
        return  accountDetails;
    }
}
