package com.EnergyUtilityApp.services;

import com.EnergyUtilityApp.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.EnergyUtilityApp.dtos.AccountDTO;
import com.EnergyUtilityApp.dtos.DeviceDTO;
import com.EnergyUtilityApp.dtos.builders.AccountBuilder;
import com.EnergyUtilityApp.dtos.builders.DeviceBuilder;
import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.entities.Device;
import com.EnergyUtilityApp.repositories.AccountRepository;
import com.EnergyUtilityApp.repositories.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DeviceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository ;
    private final AccountRepository accountRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository, AccountRepository accountRepository) {
        this.deviceRepository = deviceRepository;
        this.accountRepository = accountRepository;
    }


    public List<DeviceDTO> findDevices()
    {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDto)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> findEmptyDevices()
    {
        List<Device> deviceList = deviceRepository.getEmptyDevices();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDto)
                .collect(Collectors.toList());
    }



    public List<DeviceDTO> findDeviceByAccountId(UUID id) {
        List<Device> deviceList  = deviceRepository.findByAccount_Id(id);
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDto)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> findDeviceByAccountName(String name) {
        List<Device> deviceList  = deviceRepository.getDeviceByAccount_Name(name);
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDto)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(UUID id) {
        Optional<Device> deviceOptional  = deviceRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDto(deviceOptional.get());
    }

    @Transactional
    public UUID insert(DeviceDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO,null);
        Account account  = accountRepository.findById(deviceDTO.getAccountId()).get();
        device.setAccount(account);
        device = deviceRepository.save(device);

        return device.getId();
    }


    @Transactional
    public UUID insertWithUserId(DeviceDTO deviceDTO, UUID accountId) {
        Device device = DeviceBuilder.toEntity(deviceDTO,null);
        Optional<Account> account  = accountRepository.findById(accountId);
        device.setAccount(account.get());
        device = deviceRepository.save(device);

        return device.getId();
    }

    @Transactional
    public DeviceDTO update(UUID id, DeviceDTO deviceDTO) {
        Optional<Device> deviceOptional  = deviceRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }

        Device newDevice= DeviceBuilder.toEntity(deviceDTO, deviceOptional.get().getAccount());
        newDevice.setId(id);
        return DeviceBuilder.toDeviceDto(deviceRepository.save(newDevice));
    }

    @Transactional
    public DeviceDTO deleteById(UUID id )
    {
        Optional<Device> deviceOptional  = deviceRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        deviceRepository.deleteById(id);
        return DeviceBuilder.toDeviceDto(deviceOptional.get());
    }

    public List<Device> findDevicesByAccountId(UUID accountId)
    {
        return deviceRepository.findByAccount_Id(accountId);
    }
}
