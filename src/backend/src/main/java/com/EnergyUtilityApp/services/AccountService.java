package com.EnergyUtilityApp.services;

import com.EnergyUtilityApp.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.EnergyUtilityApp.dtos.AccountDTO;
import com.EnergyUtilityApp.dtos.PersonDetailsDTO;
import com.EnergyUtilityApp.dtos.builders.AccountBuilder;
import com.EnergyUtilityApp.dtos.builders.DeviceBuilder;
import com.EnergyUtilityApp.dtos.builders.PersonBuilder;
import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.entities.Device;
import com.EnergyUtilityApp.entities.Person;
import com.EnergyUtilityApp.repositories.AccountRepository;
import com.EnergyUtilityApp.repositories.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private final AccountRepository accountRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, DeviceRepository deviceRepository) {
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
    }

    @Transactional
    public List<AccountDTO> findAccounts()
    {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream()
                .map(AccountBuilder::toAccountDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public AccountDTO findAccountById(UUID id) {
        Optional<Account> accountOptional  = accountRepository.findById(id);
        if (!accountOptional.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", id);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        return AccountBuilder.toAccountDto(accountOptional.get());
    }

    @Transactional
    public UUID insert(AccountDTO accountDTO) {
        Account account = AccountBuilder.toEntity(accountDTO,null);
        account = accountRepository.save(account);
        LOGGER.debug("Account with id {} was inserted in db", account.getId());
        return account.getId();
    }

    @Transactional
    public AccountDTO update(UUID id, AccountDTO accountDTO) {
        Optional<Account> accountOptional  = accountRepository.findById(id);
        if (!accountOptional.isPresent()) {
            LOGGER.error("Account with id {} was not found in db", id);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }

        List<Device> accountDevices = deviceRepository.findByAccount_Id(id);
        Account updatedAccount = accountOptional.get();
        Account newAccount= AccountBuilder.toEntity(accountDTO,accountDevices);
        newAccount.setId(id);
        newAccount.setDeviceList(accountDevices);
        LOGGER.info("We should have created the dto {}",updatedAccount.toString());
        return AccountBuilder.toAccountDto(accountRepository.save(newAccount));
    }

    @Transactional
    public AccountDTO deleteById(UUID id )
    {
        Optional<Account> accountOptional  = accountRepository.findById(id);
        if (!accountOptional.isPresent()) {
            LOGGER.error("Could not delete. Account with id {} was not found in db", id);
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        accountRepository.deleteById(id);
        return AccountBuilder.toAccountDto(accountOptional.get());
    }
}
