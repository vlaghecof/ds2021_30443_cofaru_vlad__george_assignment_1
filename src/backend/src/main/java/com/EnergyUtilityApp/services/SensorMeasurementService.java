package com.EnergyUtilityApp.services;

import com.EnergyUtilityApp.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.EnergyUtilityApp.dtos.SensorMeasurementDTO;
import com.EnergyUtilityApp.dtos.builders.SensorMeasurementBuilder;
import com.EnergyUtilityApp.entities.Sensor;
import com.EnergyUtilityApp.entities.SensorMeasurement;
import com.EnergyUtilityApp.repositories.SensorMeasurementRepository;
import com.EnergyUtilityApp.repositories.SensorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * The class  SensorMeasurementService
 *
 * @author Cofaru Vlad
 */
@Service
public class SensorMeasurementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorMeasurementService.class);

    private final SensorMeasurementRepository sensorMeasurementRepository;
    private final SensorRepository sensorRepository;

    @Autowired
    public SensorMeasurementService(SensorMeasurementRepository sensorMeasurementRepository, SensorRepository sensorRepository) {
        this.sensorMeasurementRepository = sensorMeasurementRepository;
        this.sensorRepository = sensorRepository;
    }

    @Transactional
    public List<SensorMeasurementDTO> findSensorMeasurement() {
        List<SensorMeasurement> sensorMeasurements = sensorMeasurementRepository.findAll();
        return sensorMeasurements.stream()
                .map(SensorMeasurementBuilder::toDTO)
                .collect(Collectors.toList());
    }

    //todo ai ramas aici dute in controler dupa
    @Transactional
    public List<SensorMeasurementDTO> findSensorMeasurementByDeviceId(UUID id) {
        List<SensorMeasurement> sensorMeasurements = sensorMeasurementRepository.findBySensor_SensorDevice_Id(id);
        return sensorMeasurements.stream()
                .map(SensorMeasurementBuilder::toDTO)
                .collect(Collectors.toList());
    }



    @Transactional
    public SensorMeasurementDTO findSensorMeasurementByID(UUID id) {
        Optional<SensorMeasurement> sensorMeasurementOptional = sensorMeasurementRepository.findById(id);
        if (!sensorMeasurementOptional.isPresent()) {
            throw new ResourceNotFoundException(SensorMeasurement.class.getSimpleName() + " with id: " + id);
        }
        return SensorMeasurementBuilder.toDTO(sensorMeasurementOptional.get());
    }

    @Transactional
    public UUID insert(SensorMeasurementDTO sensorMeasurementDTO) {
        Sensor sensor  = sensorRepository.findById(sensorMeasurementDTO.getSensorID()).get();
        SensorMeasurement sensorMeasurement = SensorMeasurementBuilder.toEntity(sensorMeasurementDTO,sensor);
        sensorMeasurement = sensorMeasurementRepository.save(sensorMeasurement);
        return sensorMeasurement.getId();
    }

    @Transactional
    public UUID insertWithSensorId(SensorMeasurementDTO sensorMeasurementDTO , UUID sensorID) {
        Sensor sensor  = sensorRepository.findById(sensorID).get();
        SensorMeasurement sensorMeasurement = SensorMeasurementBuilder.toEntity(sensorMeasurementDTO,sensor);
        sensorMeasurement = sensorMeasurementRepository.save(sensorMeasurement);
        return sensorMeasurement.getId();
    }


    @Transactional
    public SensorMeasurementDTO update(UUID id, SensorMeasurementDTO sensorMeasurementDTO) {
        Sensor sensor  = sensorRepository.findById(sensorMeasurementDTO.getSensorID()).get();
        Optional<SensorMeasurement> deviceOptional = sensorMeasurementRepository.findById(id);
        System.out.println("something");
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(SensorMeasurement.class.getSimpleName() + " with id: " + id);
        }

        SensorMeasurement newSensor = SensorMeasurementBuilder.toEntity(sensorMeasurementDTO,sensor);
        newSensor.setId(id);
        return SensorMeasurementBuilder.toDTO(sensorMeasurementRepository.save(newSensor));
    }

    @Transactional
    public SensorMeasurementDTO deleteById(UUID id) {
        Optional<SensorMeasurement> deviceOptional = sensorMeasurementRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        sensorMeasurementRepository.deleteById(id);
        return SensorMeasurementBuilder.toDTO(deviceOptional.get());
    }
}
