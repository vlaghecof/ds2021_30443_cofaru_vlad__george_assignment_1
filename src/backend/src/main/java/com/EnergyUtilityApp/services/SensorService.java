package com.EnergyUtilityApp.services;

import com.EnergyUtilityApp.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.EnergyUtilityApp.dtos.SensorDTO;
import com.EnergyUtilityApp.dtos.builders.SensorBuilder;
import com.EnergyUtilityApp.entities.Account;
import com.EnergyUtilityApp.entities.Device;
import com.EnergyUtilityApp.entities.Sensor;
import com.EnergyUtilityApp.entities.SensorMeasurement;
import com.EnergyUtilityApp.repositories.DeviceRepository;
import com.EnergyUtilityApp.repositories.SensorMeasurementRepository;
import com.EnergyUtilityApp.repositories.SensorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * The class  SensorService
 *
 * @author Cofaru Vlad
 */
@Service
public class SensorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;
    private final SensorMeasurementRepository sensorMeasurementRepository;

    @Autowired
    public SensorService(DeviceRepository deviceRepository, SensorRepository sensorRepository, SensorMeasurementRepository sensorMeasurementRepository) {
        this.deviceRepository = deviceRepository;
        this.sensorRepository = sensorRepository;
        this.sensorMeasurementRepository = sensorMeasurementRepository;
    }

    @Transactional
    public List<SensorDTO> findSensors() {
        List<Sensor> sensorList = sensorRepository.findAll();
        return sensorList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }


    @Transactional
    public SensorDTO findSensorById(UUID id) {
        Optional<Sensor> sensorOptional = sensorRepository.findById(id);
        if (!sensorOptional.isPresent()) {
            throw new ResourceNotFoundException(Account.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.toSensorDTO(sensorOptional.get());
    }

    @Transactional
    public List<SensorDTO> findSensorByAccountIdId(UUID id) {
        List<Sensor> sensorList = sensorRepository.findBySensorDevice_Account_Id(id);
        return sensorList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }


    @Transactional
    public UUID insert(SensorDTO sensorDTO) {
        Sensor sensor = SensorBuilder.toEntity(sensorDTO, null,null);
        sensor = sensorRepository.save(sensor);
        return sensor.getId();
    }

    @Transactional
    public UUID insertWithDevice(UUID deviceID,SensorDTO sensorDTO) {
        Device device = deviceRepository.findById(deviceID).get();
        Sensor sensor = SensorBuilder.toEntity(sensorDTO, device,null);
        sensor = sensorRepository.save(sensor);
        return sensor.getId();
    }

    @Transactional
    public SensorDTO update(UUID id, SensorDTO sensorDTO) {
        Optional<Sensor> sensorOptional  = sensorRepository.findById(id);
        if (!sensorOptional.isPresent()) {
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }

//        Account account  = accountRepository.findById(deviceDTO.getAccountId()).get();
        List<SensorMeasurement>measurements = sensorMeasurementRepository.findBySensor_Id(id);
        Sensor newSensor= SensorBuilder.toEntity(sensorDTO, sensorOptional.get().getSensorDevice(),measurements);
        newSensor.setId(id);
        newSensor.setMeasurements(measurements);
        newSensor.setSensorDevice(sensorOptional.get().getSensorDevice());
        return SensorBuilder.toSensorDTO(sensorRepository.save(newSensor));
    }

    @Transactional
    public SensorDTO deleteById(UUID id )
    {
        Optional<Sensor> deviceOptional  = sensorRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        sensorRepository.deleteById(id);
        return SensorBuilder.toSensorDTO(deviceOptional.get());
    }


}
