package com.EnergyUtilityApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.EnergyUtilityApp.EnergyApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = EnergyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
public class Ds2020TestConfig {
    @Test
    public void contextLoads() {
        assert true ;
    }

}
