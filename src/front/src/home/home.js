import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Col, Container, Jumbotron, Modal, ModalBody, ModalHeader} from 'reactstrap';
import LoginForm from "../accounts/components/login-form"
import AccountForm from "../accounts/components/account-form";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            redirect:1,
            selected: false,
            deleted:false,
            update:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };


    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.toggleForm();
    }
    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <Button color="primary" onClick={this.toggleForm}>Log In </Button>
                        {/*<LoginForm/>*/}
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>
                    </Container>
                </Jumbotron>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Account: </ModalHeader>
                    <ModalBody>
                        <LoginForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    };
}

export default Home
