import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";
import * as API_SENSOR from "../../sensor/api/sensor-api";


const endpoint = {
    device: '/device',
    fullEndpoint:HOST.backend_api + "/device",
    host : HOST.backend_api
};

function getDevices(callback) {
    let request = new Request(HOST.backend_api + endpoint.device, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

// fetch(API_SENSOR.endpoint.host +"\/"+"device/" + "empty")
function getEmptyDevices(callback) {
    let request = new Request(HOST.backend_api +"\/"+"device/" + "empty", {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getSensorByDeviceId(params, callback){
    let request = new Request(endpoint.fullEndpoint + "\/" + params, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDeviceById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+ params, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDevice(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.clientID, {
        method: 'POST',
        headers : {
            'Authorization': localStorage.getItem('user'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putDevice(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.deviceId, {
        method: 'PUT',
        headers : {
            'Authorization': localStorage.getItem('user'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    console.log("Method: " + request.method);
    console.log("body: " + user);

    RestApiClient.performRequest(request, callback);
}

function deleteDevice(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.deviceId, {
        method: 'DELETE',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }

    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}



export {
    getDevices,
    getEmptyDevices,
    getDeviceById,
    postDevice,
    deleteDevice,
    putDevice,
    endpoint
};
