import React from "react";
import validate from "../components/validators/device-validators"
import * as API_DEVICES from "../api/device-api"
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class UpdateAccount extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);

        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            errorStatus: 0,
            error: null,
            persons: [],
            formIsValid: false,
            clientID: '',
            selectedDevice: [],
            valid: false,
            formControls: {
                description: {
                    value: '',
                    placeholder: 'Description of device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                location: {
                    value: '',
                    placeholder: 'Location of device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                max_consumption: {
                    value: '',
                    placeholder: 'Max energy of the device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                average_consumption: {
                    value: '',
                    placeholder: 'Baseline energy of device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                device_id: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleFormUpdate() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;
        const updatedFormElement = updatedControls[name];


        if(name === "device_id" && value !== '1')
        {
            this.loadDevice(value)

        }
        else
        {
            updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
            updatedFormElement.value = value;
            updatedFormElement.touched = true;

            updatedControls[name] = updatedFormElement;

            let formIsValid = updatedFormElement.valid;

            this.setState({
                formControls: updatedControls,
                formIsValid: formIsValid
            });
        }
    };


    loadDevice(device_id)
    {
        this.setState({
            formIsValid: true
        });
        fetch("http://localhost:8080/device/" + device_id)
            .then((response2) => {
                return response2.json();
            })
            .then(data => {
                this.setState({
                        device: data
                    }
                );
                console.log(this.state.device);

                document.getElementById("average_consumptionField").value = this.state.device.averageConsumption;
                document.getElementById("descriptionField").value = this.state.device.description;
                document.getElementById("max_consumptionField").value = this.state.device.maximumConsumption;
                document.getElementById("locationField").value = this.state.device.location;
            }).catch(error => {
            console.log(error);
        });
    }


    postDevice(device) {
        return API_DEVICES.putDevice(device, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted device with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let person = {
            description: document.getElementById("descriptionField").value,
            location: document.getElementById("locationField").value,
            maximumConsumption: document.getElementById("max_consumptionField").value,
            averageConsumption:  document.getElementById("average_consumptionField").value,
            deviceId:document.getElementById("deviceField").value,
        };

        console.log(person);
        this.postDevice(person);

    }

    render() {

        return (
            <div>
                <FormGroup id='device_id'>

                    <Label for='deviceField'> Device: </Label>
                    <br/>
                    <select id='deviceField'
                            name='device_id'
                            required
                            onChange={this.handleChange}
                            >
                        <option key={1} value={1}>Select Device</option>
                        {this.state.persons.map((person) => <option key={person.key} value={person.key}>{person.display}</option>)}

                    </select>
                    {this.state.formControls.device_id.touched && !this.state.formControls.device_id.valid &&
                    <div className={"error-message row"}> * You must select a device </div>}

                </FormGroup>

                <FormGroup id='description'>

                    <Label for='descriptionField'> Description: </Label>
                    <Input name='description' id='descriptionField' placeholder={this.state.formControls.description.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.description.value}
                           touched={this.state.formControls.description.touched? 1 : 0}
                           valid={this.state.formControls.description.valid}
                           required
                    />
                    {this.state.formControls.description.touched && !this.state.formControls.description.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='location'>
                    <Label for='locationField'> Location: </Label>
                    <Input name='location' id='locationField' placeholder={this.state.formControls.location.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.location.value}
                           touched={this.state.formControls.location.touched? 1 : 0}
                           valid={this.state.formControls.location.valid}
                           required
                    />
                    {this.state.formControls.location.touched && !this.state.formControls.location.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='max_consumption'>
                    <Label for='max_consumptionField'> Max Energy: </Label>
                    <Input name='max_consumption' id='max_consumptionField' placeholder={this.state.formControls.max_consumption.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.max_consumption.value}
                           touched={this.state.formControls.max_consumption.touched? 1 : 0}
                           valid={this.state.formControls.max_consumption.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='average_consumption'>
                    <Label for='average_consumptionField'> Baseline Energy: </Label>
                    <Input name='average_consumption' id='average_consumptionField' placeholder={this.state.formControls.average_consumption.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.average_consumption.value}
                           touched={this.state.formControls.average_consumption.touched? 1 : 0}
                           valid={this.state.formControls.average_consumption.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }

    componentDidMount() {
        fetch("http://localhost:8080/device")
            .then((response) => {
                return response.json();
            })
            .then(data => {
                let teamsFromApi = data.map(team => {
                    return {key: team.id, value: team.id, display: team.description}
                });
                this.setState({
                    persons:teamsFromApi
                });
            }).catch(error => {
            console.log(error);
        });
    }
}

export default UpdateAccount;
