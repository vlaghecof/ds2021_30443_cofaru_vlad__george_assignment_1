import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row} from 'reactstrap';

import * as API_ACCOUNT from "./api/account-api";
import AccountTable from "./components/account-table";
import AccountForm from "./components/account-form";
import DeleteAccount from "./components/delete-account";
import UpdateForm from "../device/components/update-form";
import UpdateAccount from "./components/update-account";
import LoginForm from "./components/login-form";


class AccountContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleDeleteForm = this.toggleDeleteForm.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            redirect:1,
            selected: false,
            deleted:false,
            update:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        console.log("testing daca avem user ", localStorage.getItem('user'))


        console.log("Din functia de GET " , localStorage)
        console.log("Role " , localStorage.getItem("roleAdmin"))
        let role = localStorage.getItem("roleAdmin");
        // debugger
        console.log(typeof role)
        console.log("Role Variabila:",role);


        if (role==="true" )
        {
            return API_ACCOUNT.getAccounts((result, status, err) => {

                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
        }
        else
        {
            window.location.href='/';

        }



        // if(  localStorage.getItem("roleAdmin")===true)
        // {
        //     console.log("Avem Admin")
        // }   else
        // {
        //     console.log("Nu e admin ")
        // }



    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    toggleDeleteForm() {
        this.setState({deleted: !this.state.deleted});
    }

    toggleUpdateForm() {
        this.setState({update: !this.state.update});
    }
    reload() {
        this.setState({
            isLoaded: false,
            deleted:false,
            selected:false,
            update:false
        });
        // this.toggleForm();
        this.fetchPersons();
    }

    render() {


        return (
            <div>
                <CardHeader>
                    <strong> Account Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Account </Button>
                            <Button color="danger" onClick={this.toggleDeleteForm}>Delete Account </Button>
                            <Button color="warning" onClick={this.toggleUpdateForm}>Update Account </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <AccountTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Account: </ModalHeader>
                    <ModalBody>
                        <AccountForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.deleted} toggle={this.toggleDeleteForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeleteForm}>Delete Account </ModalHeader>
                    <ModalBody>
                        <DeleteAccount reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.update} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}>Update Account </ModalHeader>
                    <ModalBody>
                        <UpdateAccount reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}


export default AccountContainer;
