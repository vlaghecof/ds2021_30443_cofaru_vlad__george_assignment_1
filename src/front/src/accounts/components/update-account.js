import React from "react";
import validate from "../components/validators/person-validators"
import * as API_ACCOUNT from "../api/account-api"
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class UpdateAccount extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            persons: [],
            formIsValid: false,
            clientID: '',
            selectedDevice: [],
            formControls: {
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Password ...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birth_date: {
                    value: '',
                    type: 'date',
                    placeholder: 'Birth date..',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                client_id: {
                    value: '(Select client)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;


        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];


        if (name === "client_id" && value != '1') {
            console.log(value)
            this.loadAccount(value)
            updatedFormElement.valid = true;
        } else if (name === "client_id" && value === '1') {
            console.log(value)
            this.loadAccount(value)
            updatedFormElement.valid = false;
        } else {
            updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        }
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: true
        });
        // this.loadAccount();
    };


    loadAccount(id) {

        return API_ACCOUNT.getAccountById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    selectedDevice: result
                });
                console.log(result)
                document.getElementById("nameField").value = this.state.selectedDevice.name;
                document.getElementById("passwordField").value = this.state.selectedDevice.password;
                document.getElementById("addressField").value = this.state.selectedDevice.address;
                document.getElementById("birthDateField").value = this.state.selectedDevice.birthDate;

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
        // fetch(API_ACCOUNT.endpoint.fullEndpoint +"/"+ id)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         this.setState({
        //             selectedDevice: data
        //         });
        //         console.log(data)
        //         document.getElementById("nameField").value = this.state.selectedDevice.name;
        //         document.getElementById("passwordField").value = this.state.selectedDevice.password;
        //         document.getElementById("addressField").value = this.state.selectedDevice.address;
        //         document.getElementById("birthDateField").value = this.state.selectedDevice.birthDate;
        //
        //
        //     }).catch(error => {
        //     console.log(error);
        // });
    }

    putAccount(device) {
        return API_ACCOUNT.putAccount(device, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted device with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let person = {
            name: document.getElementById("nameField").value,
            password: document.getElementById("passwordField").value,
            birthDate: document.getElementById("birthDateField").value,
            // birthDate: "2021-10-12",
            address: document.getElementById("addressField").value,
            isAdmin: false,

            clientId: this.state.selectedDevice["id"]
        };

        console.log(person);
        this.putAccount(person);
    }

    render() {

        return (
            <div>

                <FormGroup id='description'>

                    <Label for='clientField'> Client: </Label>
                    <br/>
                    <select id='clientField'
                            name='client_id'
                            required
                            onChange={this.handleChange}
                            touched={this.state.formControls.client_id.touched ? 1 : 0}>
                        <option key={1} value={1}>Select A Client</option>
                        {this.state.persons.map((person) => <option key={person.key}
                                                                    value={person.key}>{person.display}</option>)}

                    </select>
                    {this.state.formControls.client_id.touched && !this.state.formControls.client_id.valid &&
                    <div className={"error-message row"}> * You must select a client </div>}

                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched ? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid={this.state.formControls.password.valid}
                           required
                    />
                    {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                    <div className={"error-message"}> * Password must have a valid format</div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched ? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='birth_date'>
                    <Label for='birthDateField'> Birth Date: </Label>
                    <Input name='birth_date' id='birthDateField'
                           placeholder={this.state.formControls.birth_date.placeholder}
                           type={this.state.formControls.birth_date.type}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birth_date.value}
                           touched={this.state.formControls.birth_date.touched ? 1 : 0}
                           valid={this.state.formControls.birth_date.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Confirm Delete </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }

    componentDidMount() {

        return API_ACCOUNT.getAccounts((result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted device with id: " + result);
                let teamsFromApi = result.map(team => {
                    return {key: team.id, value: team.name, display: team.name}
                });
                this.setState({
                    // persons: [{key: '1', value: '', display: '(Select client)'}].concat(teamsFromApi)
                    persons: teamsFromApi
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


        // fetch(API_ACCOUNT.endpoint.fullEndpoint)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         let teamsFromApi = data.map(team => {
        //             return {key: team.id, value: team.name, display: team.name}
        //         });
        //         this.setState({
        //             // persons: [{key: '1', value: '', display: '(Select client)'}].concat(teamsFromApi)
        //             persons: teamsFromApi
        //         });
        //     }).catch(error => {
        //     console.log(error);
        // });
    }
}


export default UpdateAccount;
