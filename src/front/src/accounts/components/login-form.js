import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/account-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import validate from "../components/validators/person-validators";
import { Redirect } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
// import { useHistory } from "react-router-dom";
// let history = useHistory();

console.log(React.version);
console.log(localStorage)
class LoginForm extends React.Component {

    constructor(props) {


        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {

            errorStatus: 0,
            error: null,
            redirect:false,
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Enter username',
                    touched: false,
                },
                password: {
                    value: '',
                    type: 'password',
                    placeholder: 'Enter password',
                    touched: false,
                }
            }
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.logoutUser = this.logoutUser.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    authenticateAccount(account) {
        return API_USERS.authenticateAccount(account, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                localStorage.setItem('user', result.token);
                localStorage.setItem('accountId',result.id);
                localStorage.setItem('roleAdmin',result.admin);
                console.log("DIN AUTH " , result)
                console.log(localStorage)
                // window.location.href='/graph/'+localStorage.getItem("accountId")

                if(  result.admin ===true)
                {
                    window.location.href='/account';
                }   else
                {
                    window.location.href='/graph';
                }

                setTimeout(() => {
                    this.setState({ redirect : !this.state.redirect })
                }, 4000);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


    }

    logoutUser()
    {
        console.log("user is now logged out")
        localStorage.clear()
        console.log("after logout " + localStorage)
        console.log("size storage " + localStorage.length)

        this.setState({
            redirect:false
        })
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls,
        });

    };

    handleSubmit() {
        let account = {
            name: this.state.formControls.name.value,
            password: this.state.formControls.password.value,
        };

        console.log(account);
        this.authenticateAccount(account);

        // if (localStorage.getItem("user")!=='empty') {
        //     this.setState({redirect:true})
        // }
        // history.push("/home");
        // console.log(this.state.redirect)




    }



    render() {
        // if (this.state.redirect===true) {
        //     this.setState({redirect:false})
        //     let path = "/graph/" + localStorage.getItem("accountId")
        //     console.log(path)
        //     return <Redirect to={path} />
        // }
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField'
                           onChange={this.handleChange}
                           placeholder={this.state.formControls.name.placeholder}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField'
                           onChange={this.handleChange}
                           placeholder={this.state.formControls.password.placeholder}
                           type={this.state.formControls.password.type}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                        <Button type={"submit"} onClick={this.logoutUser}>  Logout </Button>
                    </Col>
                </Row>
            </div>
        ) ;
    }
}

export default LoginForm;
