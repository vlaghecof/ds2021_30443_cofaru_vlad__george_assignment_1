import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Password',
        accessor: 'password',
    },

    {
        Header: 'Birth Day',
        accessor: 'birthDate',
    },

    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'devices',
        accessor: 'deviceList',
    }

];

const filters = [
    {
        accessor: 'name',
    }
];

class AccountTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default AccountTable;