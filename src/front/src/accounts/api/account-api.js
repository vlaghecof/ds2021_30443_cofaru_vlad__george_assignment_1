import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    person: '/account',
    fullEndpoint: HOST.backend_api + "/account",
    host: HOST.backend_api,
    login: HOST.backend_api + "/authenticate"
};

function authenticateAccount(user, callback) {
    let request = new Request(endpoint.login, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getAccounts(callback) {
    let request = new Request(HOST.backend_api + endpoint.person, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log(localStorage.getItem('user'))
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getAccountById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.person + "/" + params, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.person, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.person + "/" + user["clientId"], {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/json',

        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    console.log("Method: " + request.method);
    console.log("body: " + user);

    RestApiClient.performRequest(request, callback);
}


function deleteAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.person + "/" + user["clientId"], {
        method: 'DELETE',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }

    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getAccounts,
    getAccountById,
    postAccount,
    deleteAccount,
    putAccount,
    authenticateAccount,
    endpoint
};
