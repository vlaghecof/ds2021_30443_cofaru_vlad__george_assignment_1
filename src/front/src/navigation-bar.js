import React from 'react'
import logo from './commons/images/favpng_electricity-logo-royalty-free.png';

import {
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    { localStorage.getItem("roleAdmin")=="false" &&
                    <DropdownMenu right >
                        <DropdownItem >
                            {/*{*/}
                            {/*    */}
                            {/*    localStorage.getItem('roleAdmin') && <NavLink href="/person">Persons</NavLink>*/}

                            {/*}*/}
                            {/*<NavLink href="/account">Accounts</NavLink>*/}
                            {/*<NavLink href="/device">Devices</NavLink>*/}
                            {/*<NavLink href="/sensor">Sensor</NavLink>*/}
                            <NavLink href="/graph">Graph</NavLink>
                        </DropdownItem>

                    </DropdownMenu>}

                    { localStorage.getItem("roleAdmin")=="true"  &&
                    <DropdownMenu right >

                        <DropdownItem >
                            {/*{*/}
                            {/*    */}
                            {/*    localStorage.getItem('roleAdmin') && <NavLink href="/person">Persons</NavLink>*/}

                            {/*}*/}
                            <NavLink href="/account">Accounts</NavLink>
                            <NavLink href="/device">Devices</NavLink>
                            <NavLink href="/sensor">Sensor</NavLink>
                            {/*<NavLink href="/graph">Graph</NavLink>*/}
                        </DropdownItem>


                    </DropdownMenu>}
                </UncontrolledDropdown>
                <Button color="danger" className="text-center" onClick={function(){ localStorage.clear(); window.location.href="/"}}>LogOut</Button>

            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
