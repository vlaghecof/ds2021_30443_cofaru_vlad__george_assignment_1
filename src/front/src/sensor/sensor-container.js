import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row} from 'reactstrap';

import * as DEVICE_ACCOUNT from "./api/sensor-api";
import SensorTable from "./components/sensor-table";
import UpdateSensor from "./components/update-sensor";
import SensorForm from "./components/sensor-form";
import DeleteSensor from "./components/delete-sensor";




class SensorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleDeleteForm = this.toggleDeleteForm.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            deleted:false,
            update:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        let role = localStorage.getItem("roleAdmin");
        if (role === "true") {
            return DEVICE_ACCOUNT.getSensors((result, status, err) => {

                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
        }
        else
        {
            window.location.href = '/';
        }

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleDeleteForm() {
        this.setState({deleted: !this.state.deleted});
    }

    toggleUpdateForm() {
        this.setState({update: !this.state.update});
    }

    reload() {
        this.setState({
            isLoaded: false,
            deleted:false,
            selected:false,
            update:false
        });
        this.fetchPersons();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Sensor Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Sensor</Button>
                            <Button color="danger" onClick={this.toggleDeleteForm}>Delete Sensor </Button>
                            <Button color="warning" onClick={this.toggleUpdateForm}>Update Sensor </Button>
                        </Col>
                    </Row>

                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <SensorTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Sensor: </ModalHeader>
                    <ModalBody>
                        <SensorForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.deleted} toggle={this.toggleDeleteForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeleteForm}>Delete Sensor </ModalHeader>
                    <ModalBody>
                        <DeleteSensor reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.update} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}>Update Device </ModalHeader>
                    <ModalBody>
                        <UpdateSensor reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default SensorContainer;
