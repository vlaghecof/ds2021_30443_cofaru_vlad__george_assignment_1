import {HOST} from "../../commons/hosts"
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    device: '/sensor',
    fullEndpoint:HOST.backend_api + "/sensor",
    host : HOST.backend_api
};

function getSensors(callback) {
    let request = new Request(HOST.backend_api + endpoint.device, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}




function getSensorsById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+ params, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postSensor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.clientID, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putSensor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.deviceId, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    console.log("Method: " + request.method);
    console.log("body: " + user);

    RestApiClient.performRequest(request, callback);
}

function deleteSensor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/"+user.deviceId, {
        method: 'DELETE',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }

    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}



export {
    getSensors,
    getSensorsById,
    postSensor,
    deleteSensor,
    putSensor,
    endpoint
};
