import React from "react";
import * as API_SENSOR from "../../sensor/api/sensor-api";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class DeleteSensor extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            devices: [],
            selectedDeviceId: '',
            selectedDevice: [],
            formIsValid: true,
            clientID: '',
            testDesc: "description",
            formControls: {
                description: {
                    value: this.testDesc,
                    placeholder: 'Description of device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                max_consumption: {
                    value: '',
                    placeholder: 'Max energy of the device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                device_id: {
                    value: '(Select client)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;

        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
            formIsValid: true
        });

        this.loadDevice()
    };

    loadDevice() {
        // fetch(API_SENSOR.endpoint.fullEndpoint + "\/" + this.state.formControls.device_id.value)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         this.setState({
        //             selectedDevice: data,
        //             testDesc: data["description"]
        //         });
        //         console.log("From loadDevice")
        //         console.log(this.state.selectedDevice);
        //         console.log(this.state.testDesc);
        //         console.log("------------------")
        //     }).catch(error => {
        //     console.log(error);
        // });

        //
        return API_SENSOR.getSensorsById(this.state.formControls.device_id.value, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    selectedDevice: result,
                    testDesc: result["description"]
                });
                console.log("From loadDevice")
                console.log(this.state.selectedDevice);
                console.log(this.state.testDesc);
                console.log("------------------")
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


    }

    deleteDevice(device) {
        return API_SENSOR.deleteSensor(device, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted device with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log("Din submit")
        console.log(this.state.formControls.device_id.value)
        let person = {
            deviceId: this.state.formControls.device_id.value,
        };

        console.log(person);
        this.deleteDevice(person);
    }

    render() {

        return (
            <div>

                <FormGroup id='device'>

                    <Label for='deviceField'> Device: </Label>
                    <br/>
                    <select id='deviceField'
                            name='device_id'
                            required
                            onChange={this.handleChange}
                    >
                        <option key={1} value={1}>Select Sensor</option>
                        {this.state.devices.map((person) => <option key={person.key}
                                                                    value={person.key}>{person.display}</option>)}

                    </select>
                    {this.state.formControls.device_id.touched && !this.state.formControls.device_id.valid &&
                    <div className={"error-message row"}> * You must select a Device </div>}
                </FormGroup>

                <FormGroup id='description'>

                    <Label for='descriptionField'> Description: </Label>
                    <Input name='description' id='descriptionField'
                           placeholder={this.state.formControls.description.placeholder}
                           value={this.state.selectedDevice["sensorDescription"]}
                           readOnly
                    />
                    {this.state.formControls.description.touched && !this.state.formControls.description.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>


                <FormGroup id='max_consumption'>
                    <Label for='max_consumptionField'> Max Energy: </Label>
                    <Input name='max_consumption' id='max_consumptionField'
                           placeholder={this.state.formControls.max_consumption.placeholder}
                           min={0} max={100} type="number"
                           value={this.state.selectedDevice["maximumValue"]}
                           readOnly
                    />
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} color="danger" disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Confirm Delete </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }

    componentDidMount() {

        return API_SENSOR.getSensors((result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                let devicesApi = result.map(team => {
                    return {key: team.id, value: team.sensorDescription, display: team.sensorDescription}
                });
                console.log(devicesApi);
                this.setState({
                    devices: devicesApi
                });
                console.log(this.state.devices);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
        // fetch(API_SENSOR.endpoint.fullEndpoint)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         let devicesApi = data.map(team => {
        //             return {key: team.id, value: team.sensorDescription, display: team.sensorDescription}
        //         });
        //         console.log(devicesApi);
        //         this.setState({
        //             devices:devicesApi
        //         });
        //         console.log(this.state.devices);
        //     }).catch(error => {
        //     console.log(error);
        // });


        return API_SENSOR.getSensorsById(this.state.formControls.device_id.value, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    selectedDevice: result
                });
                console.log("From loadDevice")
                console.log(this.state.selectedDevice);
                console.log("------------------")
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

        // fetch(API_SENSOR.endpoint.fullEndpoint + "\/" + this.state.formControls.device_id.value)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         this.setState({
        //             selectedDevice: data
        //         });
        //         console.log("From loadDevice")
        //         console.log(this.state.selectedDevice);
        //         console.log("------------------")
        //     }).catch(error => {
        //     console.log(error);
        // });
    }
}

export default DeleteSensor;
