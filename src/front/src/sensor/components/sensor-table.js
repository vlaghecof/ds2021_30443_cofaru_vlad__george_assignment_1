import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Description',
        accessor: 'sensorDescription',
    },
    {
        Header: 'Maximum Value',
        accessor: 'maximumValue',
    },

    {
        Header: 'Device Id ',
        accessor: 'deviceId',
    },

];

const filters = [
    {
        accessor: 'name',
    }
];

class SensorTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default SensorTable;