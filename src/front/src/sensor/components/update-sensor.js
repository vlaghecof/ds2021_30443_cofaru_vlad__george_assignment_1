import React from "react";
import validate from "../components/validators/device-validators"
import * as API_SENSOR from "../api/sensor-api"
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class UpdateSensor extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            persons: [],
            formIsValid: false,
            clientID: '',
            selectedSensor: [],
            selectedDevice: [],
            formControls: {
                description: {
                    value: '',
                    placeholder: 'Description of device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                max_energy: {
                    value: '',
                    placeholder: 'Max energy of the device...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                client_id: {
                    value: '(Select client)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;


        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];


        if (name === "client_id" && value != '1') {
            this.loadSensor(value);

        } else {
            updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

            updatedFormElement.value = value;
            updatedFormElement.touched = true;

            updatedControls[name] = updatedFormElement;

            let formIsValid = updatedFormElement.valid;

            this.setState({
                formControls: updatedControls,
                formIsValid: formIsValid
            });
        }
    };


    loadSensor(sensorId) {

        return API_SENSOR.getSensorsById(sensorId, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    selectedSensor: result,
                    formIsValid: true
                });
                console.log(this.state.selectedSensor);
                document.getElementById("descriptionField").value = this.state.selectedSensor.sensorDescription;
                document.getElementById("max_energyField").value = this.state.selectedSensor.maximumValue;

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

        //
        // fetch(API_SENSOR.endpoint.fullEndpoint + "\/" + sensorId)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         this.setState({
        //             selectedSensor: data,
        //             formIsValid: true
        //         });
        //         console.log(this.state.selectedSensor);
        //         document.getElementById("descriptionField").value = this.state.selectedSensor.sensorDescription;
        //         document.getElementById("max_energyField").value = this.state.selectedSensor.maximumValue;
        //
        //     }).catch(error => {
        //     console.log(error);
        // });

    }

    updateSensor(device) {
        return API_SENSOR.putSensor(device, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted device with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let person = {
            sensorDescription: document.getElementById("descriptionField").value,
            maximumValue: document.getElementById("max_energyField").value,
            deviceId: document.getElementById("clientField").value,
        };
        console.log("din Submit")
        console.log(person);
        this.updateSensor(person);
    }

    render() {

        return (
            <div>


                <FormGroup id='description'>

                    <Label for='clientField'> Sensor: </Label>
                    <br/>
                    <select id='clientField'
                            name='client_id'
                            required
                            onChange={this.handleChange}
                            touched={this.state.formControls.client_id.touched ? 1 : 0}>
                        <option key={1} value={1}>Select A Sensor</option>
                        {this.state.persons.map((person) => <option key={person.key}
                                                                    value={person.key}>{person.display}</option>)}

                    </select>
                    {this.state.formControls.client_id.touched && !this.state.formControls.client_id.valid &&
                    <div className={"error-message row"}> * You must select a client </div>}

                </FormGroup>

                <FormGroup id='description'>

                    <Label for='descriptionField'> Description: </Label>
                    <Input name='description' id='descriptionField'
                           placeholder={this.state.formControls.description.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.description.value}
                           touched={this.state.formControls.description.touched ? 1 : 0}
                           valid={this.state.formControls.description.valid}
                           required
                    />
                    {this.state.formControls.description.touched && !this.state.formControls.description.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='max_energy'>
                    <Label for='max_energyField'> Max Energy: </Label>
                    <Input name='max_energy' id='max_energyField'
                           placeholder={this.state.formControls.max_energy.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.max_energy.value}
                           touched={this.state.formControls.max_energy.touched ? 1 : 0}
                           valid={this.state.formControls.max_energy.valid}
                           required
                    />
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Confirm Update </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }

    componentDidMount() {
        return API_SENSOR.getSensors((result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                let devicesApi = result.map(team => {
                    return {key: team.id, value: team.sensorDescription, display: team.sensorDescription}
                });
                console.log(devicesApi);
                this.setState({
                    persons: devicesApi
                });
                console.log(this.state.persons);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

        // fetch(API_SENSOR.endpoint.fullEndpoint)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         let devicesApi = data.map(team => {
        //             return {key: team.id, value: team.sensorDescription, display: team.sensorDescription}
        //         });
        //         console.log(devicesApi);
        //         this.setState({
        //             persons: devicesApi
        //         });
        //         console.log(this.state.persons);
        //     }).catch(error => {
        //     console.log(error);
        // });

        return API_SENSOR.getSensorsById(this.state.formControls.client_id.value, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    selectedDevice: result
                });
                console.log("From loadDevice")
                console.log(this.state.selectedDevice);
                console.log("------------------")
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

        // fetch(API_SENSOR.endpoint.fullEndpoint + "\/" + this.state.formControls.client_id.value)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         this.setState({
        //             selectedDevice: data
        //         });
        //         console.log("From loadDevice")
        //         console.log(this.state.selectedDevice);
        //         console.log("------------------")
        //     }).catch(error => {
        //     console.log(error);
        // });
    }
}

export default UpdateSensor;
