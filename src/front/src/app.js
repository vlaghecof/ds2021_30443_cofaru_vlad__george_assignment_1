import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PersonContainer from './person/person-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import AccountContainer from "./accounts/account-container";
import Graph from "./graph/graph";
import DeviceContainer from "./device/device-container";
import SensorContainer from "./sensor/sensor-container";


class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        {/*<Route*/}
                        {/*    exact*/}
                        {/*    path='/person'*/}
                        {/*    render={() => <PersonContainer/>}*/}
                        {/*/>*/}

                        <Route
                            exact
                            path='/account'
                            render={() => <AccountContainer/>}
                        />

                        <Route
                            exact
                            path='/device'
                            render={() => <DeviceContainer/>}
                        />

                        <Route
                            exact
                            path='/sensor'
                            render={() => <SensorContainer/>}
                        />

                        <Route
                            exact
                            path='/graph'
                            render={() => <Graph/>}
                        />

                        {/*<Route*/}
                        {/*    exact*/}
                        {/*    path='/user'*/}
                        {/*    render={() => <PersonAccount/>}*/}
                        {/*/>*/}

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>

        )
    };
}

export default App
