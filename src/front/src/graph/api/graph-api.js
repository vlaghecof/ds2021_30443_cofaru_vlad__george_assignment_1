import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    measurement: '/measurement',
    fullEndpoint: HOST.backend_api + "/measurement",
    host: HOST.backend_api,
    sensors: HOST.backend_api + "/sensor/account/",
    deviceAccount: HOST.backend_api + "/device/account/",
    measurementDevice: HOST.backend_api + "/measurement/device/",
    generateMeasurements: HOST.backend_api + "/measurement/generate/"
};


function getMeasurementsPerDevice(device, callback) {
    let request = new Request(endpoint.measurementDevice + device.id, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log("url:", request.url);
    RestApiClient.performRequest(request, callback);
}

function getMeasurements(callback) {
    let request = new Request(HOST.backend_api + "/device/account/" + localStorage.getItem("accountId"), {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log("url:", request.url);
    RestApiClient.performRequest(request, callback);
}

function getSensors(callback) {
    let request = new Request(endpoint.sensors + localStorage.getItem("accountId"), {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log("url:", request.url);
    RestApiClient.performRequest(request, callback);
}


function generateMeasurementsSensors(sensor, callback) {
    let request = new Request(endpoint.generateMeasurements + sensor.id, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('user'),
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    console.log("url:", request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getMeasurementsPerDevice,
    generateMeasurementsSensors,
    getSensors,
    getMeasurements,
    endpoint
};
