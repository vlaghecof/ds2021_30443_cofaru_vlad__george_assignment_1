import React, {Component} from 'react';
import {LineSeries, XYPlot} from 'react-vis';
import XAxis from "react-vis/es/plot/axis/x-axis";
import YAxis from "react-vis/es/plot/axis/y-axis";
import '../../node_modules/react-vis/dist/style.css';
import HorizontalGridLines from "react-vis/es/plot/horizontal-grid-lines";
import VerticalGridLines from "react-vis/es/plot/vertical-grid-lines";
import {Button, Col, FormGroup, Input, Label} from "reactstrap";
import validate from "../person/components/validators/person-validators";
import "./modal/account.css"
// import { useParams } from 'react-router';
import * as API_MEASUREMENT from "./api/graph-api"
import * as API_ACCOUNT from "../accounts/api/account-api";

class Graph extends Component {

    constructor(props) {
        super(props);
        this.selectItem = this.selectItem.bind(this);
        this.selectSensor=this.selectSensor.bind(this);
        this.generateMeasurements= this.generateMeasurements.bind(this);
        this.state = {
            errorStatus: 0,
            error: null,
            data: [],
            mapped: [],
            selectedDate: 0,
            filtered: [],
            devices: [],
            selectedDevice: [],
            selectedDeviceMeasurements: [],
            loadedMeasurements: [],
            chartData: [],
            sensors:[],
            totalConsumption:0,
            selectedSensorId:0,
            formControls: {
                birth_date: {
                    value: '',
                    type: 'date',
                    placeholder: 'Birth date..',
                    valid: false,
                    touched: false,
                },

            }
        };

        this.fetchMeasurements = this.fetchMeasurements.bind(this);
        this.fetchSensors = this.fetchSensors.bind(this);
        this.logoutUser = this.logoutUser.bind(this);
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;


        let formIsValid = true;

        console.log(value)
        // this.loadMeasurements(value)


        let filteredData = this.state.selectedDeviceMeasurements.filter(function (el) {
            return el.timestamp.includes(value)
        })


        var totalConsumption = filteredData.reduce(function(_this, val) {
            return _this + val.energyConsumption
        }, 0);

        console.log("totalConsumption")
        console.log(totalConsumption)

        document.getElementById("totalConsumption").innerHTML = "Total Consumption " + totalConsumption;
        document.getElementById("textInformation").style.display="none"


        let mappedData = filteredData.map(function (order) {
            var info = {
                "x": new Date(order.timestamp),
                "y": order.energyConsumption
            }
            return info;
        });

        console.log("Se va afisa")
        console.log(mappedData)
        this.setState({
            totalConsumption:totalConsumption,
            chartData:mappedData,
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    selectItem(param) {
        console.log(param)
        console.log(param.target)
        console.log(param.target.id)
        console.log("inner html")

        document.getElementById("detail").innerHTML = "Device id:" + param.target.innerHTML;
        document.getElementById("textInformation").style.display = "block";
        document.getElementById("textInfo").classList.add("mystyle");
        document.getElementById("textInformation").innerHTML = "Select a Date!"
        document.getElementById("totalConsumption").innerHTML = ""
        document.getElementById("birthDateField").value=''

        let device = {
            id:   param.target.id
        };
        debugger
        return API_MEASUREMENT.getMeasurementsPerDevice(device,(result, status, err) => {
            // console.log("fetch Measurements",result)
            if (result !== null && status === 200) {
                this.setState({
                    chartData:[],
                    selectedDeviceMeasurements: result
                });
                console.log("selected device measurements")
                console.log(this.state.selectedDeviceMeasurements)
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

    }

    selectSensor(param) {
        console.log(param.target)
        console.log(param.target.id)

        document.getElementById("measurementGeneration").innerHTML = "You have selected Sensor " + param.target.innerHTML;
        // document.getElementById("textInformation").style.display="none"

        this.setState({
            selectedSensorId:param.target.id
        })
        console.log("Saved Sensor Id ",this.state.selectedSensorId)
    }

    render() {

        return (
            <section id="portfolio">
                <div className="col-3 master">
                    <Button color="warning" className="text-center" onClick={this.generateMeasurements}>Read Today Measurements</Button>
                    <ul className="slides">
                        <li className="master-item">
                            <a>Devices :</a>
                        </li>
                        {this.state.devices.map((device) =>
                            <li className="master-item" key={device.id}>
                                <a id={device.id} onClick={this.selectItem}>{device.description}</a>
                            </li>)}
                    </ul>

                    <ul className="slides">

                        <li className="master-item-red">
                            <a>Sensors:</a>
                        </li>
                        {this.state.sensors.map((device) =>
                            <li className="master-item" key={device.id}>
                                <a id={device.id} onClick={this.selectSensor}>{device.sensorDescription}</a>
                            </li>)}
                    </ul>
                    {/*<Button color="danger" className="text-center" onClick={this.logoutUser}>LogOut</Button>*/}

                </div>

                <div className="col-9 detail">

                    <div id="textInfo">
                        <h1 className="detail-title text-center" id="textInformation"> Select a device ! </h1>
                    </div>

                    <h1 id="detail" className="detail-title text-center"></h1>
                    <h1 id="totalConsumption" className="detail-title text-center"></h1>
                    <h1 id="measurementGeneration" className="detail-title text-center"></h1>

                    <FormGroup id='birth_date'>
                        <Label for='birthDateField'> Date: </Label>
                        <Input name='birth_date' id='birthDateField'
                               placeholder={this.state.formControls.birth_date.placeholder}
                               type={this.state.formControls.birth_date.type}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.birth_date.value}
                               touched={this.state.formControls.birth_date.touched ? 1 : 0}
                               valid={this.state.formControls.birth_date.valid}
                               required
                        />
                    </FormGroup>


                    <XYPlot
                        xType="time"
                        width={1000}
                        height={300}>
                        <HorizontalGridLines/>
                        <VerticalGridLines/>
                        <XAxis title="Time Axis"/>
                        <YAxis title="Consumption Axis"/>
                        {/*{this.state.persons.map((person) => <option key={person.key} value={person.key}>{person.display}</option>)}*/}
                        <LineSeries
                            data={this.state.chartData}/>
                    </XYPlot>


                </div>

            </section>

        );
    }


    logoutUser()
    {
        console.log("user is now logged out")
        localStorage.clear()
        console.log("after logout " + localStorage)
        console.log("size storage " + localStorage.length)
        window.location.href='/';

    }

    generateMeasurements()
    {
        document.getElementById("birthDateField").value=''
        document.getElementById("measurementGeneration").innerHTML = "";
        // console.log(API_MEASUREMENT.endpoint.generateMeasurements+this.state.selectedSensorId)
        let sensor = {
            id:    this.state.selectedSensorId
        };

        return API_MEASUREMENT.generateMeasurementsSensors(sensor,(result, status, err) => {
            // console.log("fetch Measurements",result)
            if (result !== null && status === 200) {
                console.log("generated")
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

        // fetch(API_MEASUREMENT.endpoint.generateMeasurements+this.state.selectedSensorId)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         // let teamsFromApi = data.map(team => {
        //         //     return {key: team.id, value: team.id, display: team.description}
        //         // });
        //         console.log("Generated data for sensor ", data)
        //
        //     }).catch(error => {
        //     console.log(this.state.data);
        // });
    }


    fetchMeasurements() {
        // console.log("testing daca avem user ", localStorage.getItem('user'))

        return API_MEASUREMENT.getMeasurements((result, status, err) => {
            console.log("fetch devices",result)
            if (result !== null && status === 200) {
                this.setState({
                    devices: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchSensors()
    {
        return API_MEASUREMENT.getSensors((result, status, err) => {
            console.log("fetch devices",result)
            if (result !== null && status === 200) {
                console.log("RETURNRED Sensors", result)
                this.setState({
                    sensors: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    componentDidMount() {

        console.log("pathName")
        console.log(window.location.pathname.split("/")[2])
        let id = window.location.pathname.split("/")[2]

        // this.fetchMeasurements();

        // let role = localStorage.getItem("roleAdmin");
        // if (role === "true") {
        //
        // }

        this.fetchMeasurements();
        // fetch(API_MEASUREMENT.endpoint.deviceAccount+id)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         // let teamsFromApi = data.map(team => {
        //         //     return {key: team.id, value: team.id, display: team.description}
        //         // });
        //         console.log("RETURNRED devices", data)
        //         this.setState({
        //             devices: data
        //         });
        //     }).catch(error => {
        //     console.log(this.state.data);
        // });
        //
        // fetch(API_MEASUREMENT.endpoint.sensors+id)
        //     .then((response) => {
        //         return response.json();
        //     })
        //     .then(data => {
        //         // let teamsFromApi = data.map(team => {
        //         //     return {key: team.id, value: team.id, display: team.description}
        //         // });
        //         console.log("RETURNRED Sensors", data)
        //         this.setState({
        //             sensors: data
        //         });
        //     }).catch(error => {
        //     console.log(this.state.data);
        // });

        this.fetchSensors();
    }
}

export default Graph;