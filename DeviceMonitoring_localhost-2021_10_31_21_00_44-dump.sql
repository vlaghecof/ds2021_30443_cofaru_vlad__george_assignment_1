--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DeviceMonitoring; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "DeviceMonitoring" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Europe.1252';


ALTER DATABASE "DeviceMonitoring" OWNER TO postgres;

\connect "DeviceMonitoring"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account (id, address, birth_date, admin, name, password) FROM stdin;
\\x2947f6321e3c4a638893365d2d99d12d	undeva	2021-10-12	t	admin	admin
\\xdea15474c2af4041ae77d3b66479ad3c	Strada Memorandumului 28, Cluj-Napoca 400114	1999-04-07	f	vlad	vlad
\\x97db8d1671c349709de68af3fac2dddf	Unde vrea el	2016-06-30	f	test	test
\.


--
-- Data for Name: device; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.device (id, average_consumption, description, location, max_consumption, account_id) FROM stdin;
\\xe7209ca00c534d01a61a1dccd2b752d3	32	Tv 	Bedroom	213	\\xdea15474c2af4041ae77d3b66479ad3c
\\x9d23ffa3e5bd41ad81ed4220b97a8768	32	Laptop	Desk	123	\\xdea15474c2af4041ae77d3b66479ad3c
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, address, age, name) FROM stdin;
\.


--
-- Data for Name: sensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensor (id, maximum_value, sensor_description, device_id) FROM stdin;
\\x05354c936f094a5ca352a860915734fb	123	Tv Sensor	\\xe7209ca00c534d01a61a1dccd2b752d3
\\xaeed3269eb8d45a2892990249cc288bf	223	Laptop Sensor	\\x9d23ffa3e5bd41ad81ed4220b97a8768
\.


--
-- Data for Name: sensor_measurement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensor_measurement (id, energy_consumption, time_stamp, sensor_id) FROM stdin;
\\x9a9a18e20694480cbf83134ab18b2e09	99	2021-10-30 15:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x7a7cbab977754da790f6d9699c85c3a1	67	2021-10-30 14:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\xc0cfae0d1c8d4ec789651a21de3358d3	63	2021-10-30 13:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x03cdffee9531497c8975c74f7ff57e88	44	2021-10-30 12:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\xabca2b4257214d618e8adbc8f1981eed	55	2021-10-30 11:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x171b8e327c24425b9ba9c54b2c8b5b4b	50	2021-10-30 10:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\xbb4eec1b18f54f5baec3aee407d58a88	9	2021-10-30 09:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x203dd32618ba49858baa5c1e7c24db7a	96	2021-10-30 08:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x32a5973598f1404f867d5fd506ee3b3f	47	2021-10-30 07:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\x1eeb47643d404c388974ff1cce326ef5	43	2021-10-30 06:57:12.698224	\\x05354c936f094a5ca352a860915734fb
\\xefc47382b083482282f5c9a056c0973e	6	2021-10-30 16:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\xaa6dff84125c4b3b9c8f9e3679712bdd	52	2021-10-30 15:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\xca7fcd7facfe44c5990c4bf7a7c0c3ed	29	2021-10-30 14:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\xa2532b70bc254e2cab5dffdae1a86ffb	41	2021-10-30 13:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\xdf04af8fa56f462da74df6273f38f750	77	2021-10-30 12:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x0f4f1a37f2f44a1787e1018c7dbaa291	55	2021-10-30 11:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x0702bdb946fa4f1aa614231235f8c3d5	79	2021-10-30 10:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x47beed661871444d8f8b170174f9fffe	52	2021-10-30 09:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x7cf211b1deea4b779fd6f59d3989c463	62	2021-10-30 08:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x3318818d1962495487fa4f5d75d0ff59	44	2021-10-30 07:55:54.948447	\\xaeed3269eb8d45a2892990249cc288bf
\\x0d02c0de637846758000c6150de701f0	21	2021-10-31 15:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\xb800c035042e47f3b7e50d5ad5305513	49	2021-10-31 14:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x0917ef766f094990a573affd4bef13eb	88	2021-10-31 13:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\xb05204b498184c81b74d89bd28d241ba	58	2021-10-31 12:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x93756b96db8b43a69ede938ba78895b2	4	2021-10-31 11:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x16d1ba22344b4479a9ba03d8ab1ce443	65	2021-10-31 10:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\xb99f7624c7504bbcbe361a7b6fba994f	6	2021-10-31 09:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x839cbb2288004b0bb9fb8cdf3f7ba5a2	66	2021-10-31 08:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x51fdd2c471e046d181badce0afaf7ff8	4	2021-10-31 07:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\x82ffe8726d304f3a8cb4e457794a202b	8	2021-10-31 06:02:20.519361	\\xaeed3269eb8d45a2892990249cc288bf
\\xc7753e93e44547dc8932574ce4527a01	70	2021-10-31 15:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x5e942c0ffd3b45faada0af3abf2ae083	48	2021-10-31 14:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x3526a82a5c3047998198bf5ebf7efa12	68	2021-10-31 13:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x374a512086de42939fb14913fe8f258a	68	2021-10-31 12:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\xeb90aee8cb0244988a4939b6ef8a4262	21	2021-10-31 11:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x2dbfd3d548f84862a6d8f1ff6bb028a1	85	2021-10-31 10:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x511a0ebb3fee49d3aabcf1e094578a12	84	2021-10-31 09:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\xb171823d461840ecbc7738d54f709b56	35	2021-10-31 08:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\xcc7a767d525149faac93afc15f639008	19	2021-10-31 07:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\\x658764bd411547f0a1fbeab420aba633	51	2021-10-31 06:08:53.807951	\\x05354c936f094a5ca352a860915734fb
\.


--
-- PostgreSQL database dump complete
--

