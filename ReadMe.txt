Go to the application.properties file and customize the Database configuration to fit your database.
	eq : 	
		###############################################
		### DATABASE CONNECTIVITY CONFIGURATIONS ###
		###############################################
		database.ip = ${DB_IP:localhost}
		database.port = ${DB_PORT:5432}
		database.account = ${DB_USER:postgres}
		database.password = ${DB_PASSWORD:admin}
		database.name = ${DB_DBNAME:UsedDb}
	
	For the first time running the app , you must change the dll.auto property from validate to create, in order to create the database 
	eq:
		spring.jpa.hibernate.ddl-auto = create

	For deployment go to the gitlab-ci.yml file and comment the last line of the deploy stage saying only : production and for using Heroku you also need 2 variables to be set in the deploy stage, namely 
	--app=your_heroku_app_name  --api-key=$HEROKU_API_KEY

	The HEROKU_API_KEY must be set independently in the Gitlab repository page


In order to run the React app , simply run npm start in a cmd open to the project file location


